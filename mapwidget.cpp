#include "mapwidget.h"
#include <QVBoxLayout>
#include <QDir>
#include <QWebView>
#include <QHeaderView>
#include <QMessageBox>
#include <QStringListModel>
#include <QStringList>
#include "ui_mapwidget.h"
#include "map.h"
#include "misterxclient.h"

#include <QDebug>

MapWidget::MapWidget(QWidget *parent, Qt::WFlags flags)
    : QWidget(parent, flags),map_ui(new Ui::MapWidget)
{
    map_ui->setupUi(this);
    map_ui->map->show();
    map_ui->map->lower();

    map_ui->clearButton->hide();
    map_ui->zoomGlobal->hide();
    map_ui->zoomUser->hide();


}

MapWidget::~MapWidget()
{

}


void MapWidget::on_defineButton_clicked()
{
    map_ui->defineButton->hide();
    map_ui->zoomMinusButton->hide();
    map_ui->zoomPlusButton->hide();

    map_ui->clearButton->show();
    map_ui->zoomGlobal->show();
    map_ui->zoomUser->show();

    definedOverviewZoomingLevel = map_ui->map->showZoomLevel();

    emit areaIsDefined();

}

void MapWidget::on_clearButton_clicked()
{
    this->close();
    emit giveUp();
}

void MapWidget::on_zoomUser_clicked()
{
   map_ui->map->setZoomLevel(15); //Bird-view for google maps
   map_ui->map->setZoomingMode(USERVIEWZOOMING);
   map_ui->map->loadCoordinates();
}


void MapWidget::on_zoomGlobal_clicked()
{
    map_ui->map->setZoomLevel(definedOverviewZoomingLevel);
    map_ui->map->setZoomingMode(OVERVIEWZOOMING);
    map_ui->map->loadCoordinates();
}


void MapWidget::on_zoomMinusButton_clicked()
{
    map_ui->map->zoomOut();
    map_ui->map->setZoomingMode(DEFININGAREA);
    map_ui->map->loadCoordinates();
}


void MapWidget::on_zoomPlusButton_clicked()
{
    map_ui->map->zoomIn();
    map_ui->map->setZoomingMode(DEFININGAREA);
    map_ui->map->loadCoordinates();
}











