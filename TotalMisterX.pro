# -------------------------------------------------
# Project created by QtCreator 2009-11-11T13:13:13
# -------------------------------------------------
QT += network \
    webkit \
    xml
TARGET = MisterX
TEMPLATE = app
SOURCES += main.cpp \
    misterxwindow.cpp \
    misterxclient.cpp \
    mainwindow.cpp \
    misterxengine.cpp \
    MisterxUser.cpp \
    misterxsettingsform.cpp \
    map.cpp \
    mapwidget.cpp
HEADERS += misterxwindow.h \
    misterxclient.h \
    mainwindow.h \
    misterxengine.h \
    MisterxUser.h \
    misterxsettingsform.h \
    map.h \
    mapwidget.h
FORMS += misterxwindow.ui \
    mainwindow.ui \
    misterxsettingsform.ui \
    mapwidget.ui
OTHER_FILES += 
LIBS += qxmpp/source/debug/libQXmppClient_d.a
