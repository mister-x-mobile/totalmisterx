#ifndef MISTERXWINDOW_H
#define MISTERXWINDOW_H

#include <QMainWindow>
#include "mainwindow.h"
#include "misterxsettingsform.h"
#include "mapwidget.h"
#include <QGraphicsItem>

namespace Ui {
    class MisterXWindow;
}

class MisterXWindow : public QWidget {
    Q_OBJECT
public:
    MisterXWindow(QWidget *parent = 0);
    ~MisterXWindow();
    QTimer* paintTimer;
public slots:
    void on_hostButton_clicked();
    void on_joinButton_clicked();

protected:
    void changeEvent(QEvent *e);
    void paintEvent(QPaintEvent* event);

private:
    Ui::MisterXWindow *ui;
    MainWindow *settings;
    MisterXSettingsForm *config;
    MapWidget *game;
    bool host;
    bool connected;

    //////////////paint////////////

    long angle;
    QGraphicsRectItem *state;



private slots:
    void on_settingsButton_clicked();
    void showMap();
    void startSettings();
    void stopSettings();
    void reconnect();

};

#endif // MISTERXWINDOW_H
