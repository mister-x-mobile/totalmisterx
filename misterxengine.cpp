#include "misterxengine.h"
#include <QTimer>
#include <QPointF>
#include <iostream>
#include <QDebug>
#include <QHash>


MisterxEngine::MisterxEngine(QObject *parent)
:QXmppInvokable(parent)
{
    localUser= new MisterxUser(this);
    updateTimer = new QTimer();
    updateTimer->setInterval(5000);
    qsrand(QTime::currentTime().msec());
    //updateTimer->stop();
    connect(updateTimer,SIGNAL(timeout()), this, SLOT(timerEnd()));
}

MisterxEngine::~MisterxEngine()
{


}

bool MisterxEngine::isAuthorized( const QString &jid ) const
{
    return true;
}

void MisterxEngine::rupdateSettings(QString playerString, QString area, QString time)
{
    this->playerList=playerString.split(" ",QString::SkipEmptyParts);
    int i=1;
    foreach(QString player,playerList){
        if(player==localUser->jid){
            playerhash.insert(player,localUser);
        }
        else{
            MisterxUser * user=new MisterxUser;
            playerhash.insert(player,user);
        }
        playerhash[player]->jid=player;
        playerhash[player]->coordinates.clear();
        playerhash[player]->coordinates.append((float)qrand()/RAND_MAX*99.0);
        playerhash[player]->coordinates.append((float)qrand()/RAND_MAX*99.0);
        if(i) {
            playerhash[player]->role=MisterxUser::Misterx;
            i=0;
        }
        else  playerhash[player]->role=MisterxUser::Player;
    }
    qDebug() << "Settings erhalten" ;
}

void MisterxEngine::rupdateGameDat(QString jid,QString coordinates)
{
    qDebug() << "Daten erhalten";
    if(!coordinates.isEmpty()){
        QStringList coordtable=coordinates.split(" ",QString::SkipEmptyParts);
        playerhash[jid]->coordinates.clear();
        foreach(QString coord,coordtable){
            playerhash[jid]->coordinates.append(coord.toDouble());

        }

    }
      emit gameUpdated(playerhash[jid],this->localUser->jid);
}

void MisterxEngine::rstartGame()
{
    updateTimer->start();
    emit gameStarted();
}
void MisterxEngine::rstopGame(QString win)
{
    updateTimer->stop();
    emit gameStoped();
}

void MisterxEngine::timerEnd(void)
{
    //TODO: GPS daten auslesen und in local user speichern.

    localUser->coordinates.clear();

    // GPS data from Area: Lausanne - radnomized (46.51-46.55; 6.62-6.66)
    float LatGPS = (qrand() % 40000) +1;
    float LngGPS = (qrand() % 40000) +2;

    LatGPS = LatGPS/1000000 + 46.5;
    LngGPS = LngGPS/1000000 + 6.6;


    localUser->coordinates.append(LatGPS);
    localUser->coordinates.append(LngGPS);


    QString coordinatesString;
    foreach(double coord,localUser->coordinates){
    coordinatesString.append(QString::number(coord));
    coordinatesString.append(" ");
    }
    emit timerEnded(coordinatesString);
}

