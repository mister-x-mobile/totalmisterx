#include "misterxwindow.h"
#include "ui_misterxwindow.h"
#include "mapwidget.h"
#include "mainwindow.h"
#include "misterxsettingsform.h"
#include "map.h"


MisterXWindow::MisterXWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MisterXWindow)
{
    connected=0;
    angle=0;
    ui->setupUi(this);
    QCoreApplication::setOrganizationName("MisterX");
    QCoreApplication::setOrganizationDomain("epfl.ch");
    QCoreApplication::setApplicationName("mister-x-mobile");
    settings=new MainWindow();
    config= new MisterXSettingsForm();
    game=new MapWidget();
    connect(settings->engine,SIGNAL(gameStarted()),this,SLOT(showMap()));
    connect(settings->engine,SIGNAL(gameUpdated(MisterxUser*,QString)),game->map_ui->map,SLOT(gotUser(MisterxUser*,QString)));
    connect(settings->client,SIGNAL(connected()),this,SLOT(startSettings()));
    connect(settings->client,SIGNAL(disconnected()),this,SLOT(stopSettings()));
    connect(game,SIGNAL(giveUp()),this,SLOT(reconnect()));
    connect(settings, SIGNAL(startButtonClicked()),game, SLOT(show()) );
    connect(game, SIGNAL(areaIsDefined()), settings, SLOT(startGame()));
////////////////////////////painter//////////////////
    paintTimer = new QTimer(this);
    connect(settings->engine,SIGNAL(gameStarted()),this->paintTimer,SLOT(stop()));
    connect(paintTimer, SIGNAL(timeout()), this, SLOT(update()));
    paintTimer->start(50);
    ui->stateImage->setBackgroundBrush(Qt::black);
    ui->stateImage->setRenderHint(QPainter::Antialiasing);
    state=new QGraphicsRectItem(10,20,30,40);
    state->setBrush(Qt::white);
}

MisterXWindow::~MisterXWindow()
{
    delete ui;
    delete settings;
}

void MisterXWindow::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MisterXWindow::on_hostButton_clicked()
{
    host=1;
    if(connected){
        settings->client->disconnect();
        state->setBrush(Qt::white);
    }
    else{
        settings->connectClient();
        connected=1;
        state->setBrush(Qt::red);
    }
}

void MisterXWindow::on_joinButton_clicked()
{
    host=0;
    if(connected){
        settings->client->disconnect();
        state->setBrush(Qt::white);
    }
    else{
        settings->connectClient();
        connected=1;
        state->setBrush(Qt::red);
    }
}

void MisterXWindow::on_settingsButton_clicked()
{
    config->show();
}

void MisterXWindow::showMap()
{
    game->show();
}

void MisterXWindow::startSettings()
{
    if(host){
        settings->show();
        state->setBrush(Qt::blue);
        ui->joinButton->hide();
    }
    else{
        ui->joinButton->setText("Give up");
    }
    ui->hostButton->hide();
    ui->settingsButton->hide();

}
void MisterXWindow::stopSettings()
{
    if(host){
        settings->hide();
        ui->joinButton->show();
    }
    else{
        ui->joinButton->setText("Hunter");
    }
    connected=0;
    ui->hostButton->show();
    ui->settingsButton->show();
    state->setBrush(Qt::white);
}

void MisterXWindow::reconnect()
{
    settings->client->disconnect();
    settings->engine->updateTimer->stop();
    this->paintTimer->start();
}

void MisterXWindow::paintEvent(QPaintEvent* event){
    state->translate(width() / 2, height() / 2);
    state->rotate(angle++);
    QGraphicsScene *scene=new QGraphicsScene;
    scene->addItem(state);
    ui->stateImage->setScene(scene);


}

