#ifndef MISTERXUSER_H
#define MISTERXUSER_H

#include <QModelIndex>
#include <QPointF>
#include <QObject>
#include <QRectF>
#include <QTime>


class MisterxUser : public QObject
{  
          Q_OBJECT

public:	         
          MisterxUser(QObject* parent=0);
          ~MisterxUser();
          enum Role {Player,Misterx,Spectator};
          QString jid;
          QList<double> coordinates;
          Role role;
}; 

#endif // MISTERXUSER_H
