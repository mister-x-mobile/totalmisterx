#include "map.h"
#include "MisterxUser.h"
#include "misterxengine.h"
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDomDocument>
#include <QDomElement>
#include <QWebFrame>
#include <QWebPage>
#include <QEventLoop>
#include <QApplication>
#include <QFile>
#include <QChar>
#include <QMessageBox>
#include <QDebug>


#include <math.h>
#include <iostream>




Map::Map(QWidget *parent) : QWebView(parent), pendingRequests(0)
{
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(replyFinished(QNetworkReply*)));
    connect(this,SIGNAL(reloadMap()),
            this,SLOT(loadCoordinates()));

    zoomingLevel = 15;
    zoomingMode = OVERVIEWZOOMING;
    users.clear();


    //load(QUrl(QFile("index.html")));
}

void Map::geoCode(const QString& address)
{
    QString requestStr( tr("http://maps.google.com/maps/geo?q=%1&output=%2&key=%3")
            .arg(address)
            .arg("csv")
            .arg("GOOGLE_MAPS_KEY") );

    manager->get( QNetworkRequest(requestStr) );
    ++pendingRequests;
}

void Map::replyFinished(QNetworkReply *reply)
{
  /*  QString replyStr( reply->readAll() );
    QStringList coordinateStrList = replyStr.split(",",QString::SkipEmptyParts);

    if( coordinateStrList.size() == 4) {
        QPointF coordinate( coordinateStrList[2].toFloat(),coordinateStrList[3].toFloat() );
        userCoordinates << coordinate;
    }
*/

    --pendingRequests;
    if( pendingRequests<1 )
        emit( reloadMap() );
}

void Map::loadCoordinates()
{
    QStringList scriptStr;
    scriptStr
            << "var map = new GMap2(document.getElementById(\"map\"));"
            << "var bounds = new GLatLngBounds;"
            << "var markers = [];";



    scriptStr
            << "var misterXIcon = new GIcon(G_DEFAULT_ICON);"
            << "misterXIcon.image = \"http://www.google.com/mapfiles/markerX.png\";"
            << "misterXMarker = { icon:misterXIcon };"
            << "var userIcon = new GIcon();"
            << "userIcon.image = \"http://labs.google.com/ridefinder/images/mm_20_blue.png\";"
            << "userIcon.shadow = \"http://labs.google.com/ridefinder/images/mm_20_shadow.png\";"
            << "userIcon.iconSize = new GSize(12, 20);"
            << "userIcon.shadowSize = new GSize(22, 20);"
            << "userIcon.iconAnchor = new GPoint(6, 20);"
            << "userIcon.infoWindowAnchor = new GPoint(5, 1);"
            << "userMarker = { icon:userIcon };";


    // first point in userCoordinates is MisterX, therefore we assign a special icon to it
    // TODO: check if message ID == MisterX id, then redraw Map
    if (!users.isEmpty())
    {


        int num=0;
        int i=0;
        QString roleString="";
        foreach (MisterxUser *user, users)
        {

            if(user->role == MisterxUser::Misterx) {
                roleString = "misterXMarker";
            } else {
                roleString = "userMarker";
            }
            scriptStr << QString("markers[%1] = new GMarker(new GLatLng(%2, %3), %4);")
                                 .arg(num++)
                                 .arg(user->coordinates.at(0))
                                 .arg(user->coordinates.at(1))
                                 .arg(roleString);
        }

    }

    scriptStr
            << "for( var i=0; i<markers.length; ++i ) {"
            << "   bounds.extend(markers[i].getPoint());"
            << "   map.addOverlay(markers[i]);"
            <<    "}"
            <<    "map.setCenter(bounds.getCenter());";

    QString zoomingString; // << "map.setCenter( new GLatLng(0,0),1 );";
    //zoomingString = QString("map.setCenter( new GLatLng(0,0),%1 );").arg(zoomingLevel);
    zoomingString = zoomingModeString();
    qDebug("Zooming mode: %d", zoomingMode);
    scriptStr << zoomingString;

            //<< "map.addControl(new GLargeMapControl());" // for fadenkreuz for big touchscreens


    qDebug() << scriptStr;
    this->page()->mainFrame()->evaluateJavaScript( scriptStr.join("\n") );
}

void Map::clearCoordinates()
{
    users.clear();
}



void Map::zoomIn()
{
    ++zoomingLevel;
}



void Map::zoomOut()
{
    --zoomingLevel;
}


int Map::showZoomLevel()
{
    return zoomingLevel;
}


void Map::setZoomLevel(int zoomValue)
{
    zoomingLevel = zoomValue;
}



void Map::setZoomingMode(int zoomingModeToSet)
{
    zoomingMode = zoomingModeToSet;
    std::cout << "zooming mode set to: " << zoomingMode << endl;

}


QString Map::zoomingModeString()
{
    if (zoomingMode == USERVIEWZOOMING)
    {
        foreach(MisterxUser *user, users){
            if(user->jid==ownjid){
               qDebug("in map mode: zoom-user");
               return QString("map.setCenter( new GLatLng(%1,%2),%3 );").arg(user->coordinates.at(0)).arg(user->coordinates.at(1)).arg(zoomingLevel);
            }
        }

        qDebug("ownuser not found: MisterX");
        return QString("map.setCenter( new GLatLng(%1,%2),%3 );").arg(users.at(0)->coordinates.at(0)).arg(users.at(0)->coordinates.at(1)).arg(zoomingLevel);

    }
    else if ((zoomingMode == OVERVIEWZOOMING))
    {
        return QString("map.setCenter( new GLatLng(%1,%2),%3 );").arg(users.at(0)->coordinates.at(0)).arg(users.at(0)->coordinates.at(1)).arg(zoomingLevel);



    }
    else if ((zoomingMode == DEFININGAREA))
    {
        // Lausanne geo-code 46.51-46.55; 6.62-6.66
        return QString("map.setCenter( new GLatLng(%1,%2),%3 );").arg(46.53).arg(6.64).arg(zoomingLevel);
    }
    else // Error handling: global world will be displayed
    {
        qDebug("FATAL ERROR in map zooming mode");
        return QString("map.setCenter( new GLatLng(%1,%2),%3 );").arg(0).arg(0).arg(0);

    }



}


void Map::gotUser(MisterxUser *user,QString jid)
{
    int i=0;
    ownjid=jid;
    foreach(MisterxUser *player,users){
        if(player->jid==user->jid){
            player=user;
            i=1;
            qDebug() << "Old Player";
        }
    }
    if(i==0){
        users.append(user);
        QString adress;
        adress.append(QString::number(user->coordinates.at(0)));
        adress.append(", ");
        adress.append(QString::number(user->coordinates.at(1)));
        geoCode(adress);
        qDebug() << "New Player";
    }

    loadCoordinates();


}





