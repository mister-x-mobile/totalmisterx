#include "misterxsettingsform.h"
#include "ui_misterxsettingsform.h"

#include <QSettings>
#include "qxmpp/source/QXmppConfiguration.h"

MisterXSettingsForm::MisterXSettingsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MisterXSettingsForm)
{
    ui->setupUi(this);
    QSettings settings;
    QXmppConfiguration config;
    config.loadFrom(&settings);
    QString jid = config.getUser()+"@"+config.getDomain();
    ui->jidEdit->setText(jid);
    ui->pwdEdit->setText(config.getPasswd());
    ui->portEdit->setText(QString::number(config.getPort()));
    ui->hostEdit->setText(config.getHost());
}

MisterXSettingsForm::~MisterXSettingsForm()
{
    delete ui;
}

void MisterXSettingsForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MisterXSettingsForm::on_saveButton_clicked()
{
    QSettings settings;
    QXmppConfiguration config;
    QStringList jid = ui->jidEdit->text().split("@");
    config.setUser(jid[0]);
    config.setDomain(jid[1]);
    config.setPasswd(ui->pwdEdit->text());
    config.setPort(ui->portEdit->text().toInt());
    config.setHost(ui->hostEdit->text());
    config.saveTo(&settings);
    settings.sync();
    close();
}

void MisterXSettingsForm::on_cancelButton_clicked()
{
    close();
}
