#ifndef MAPWIDGET_H
#define MAPWIDGET_H

#include <QtGui/QWidget>
#include "ui_mapwidget.h"
#include <QWebView>
#include <QStandardItemModel>
#include "map.h"

namespace Ui {
    class MapWidget;
}

class MapWidget : public QWidget
{
    Q_OBJECT

public:
    MapWidget(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~MapWidget();
    Ui::MapWidget *map_ui;

public slots:
    void on_defineButton_clicked();
    void on_clearButton_clicked();
    void on_zoomUser_clicked();
    void on_zoomGlobal_clicked();
    void on_zoomMinusButton_clicked();
    void on_zoomPlusButton_clicked();

signals:
    void giveUp();
    void areaIsDefined();

private:
    bool enableUIFlag;
    int definedOverviewZoomingLevel;
};

#endif // MAPWIDGET_H
