#ifndef MAP_H
#define MAP_H
#include "MisterxUser.h"

#include <QWebView>

#define OVERVIEWZOOMING 0
#define USERVIEWZOOMING 1
#define DEFININGAREA 2


class QNetworkAccessManager;

class Map : public QWebView
{
    Q_OBJECT

    public:
        Map(QWidget *parent=0);
        QList<MisterxUser*> users;


public slots:
        void replyFinished(QNetworkReply*);
        void loadCoordinates();
        void geoCode(const QString &address);
        void clearCoordinates();
        void zoomIn();
        void zoomOut();
        int showZoomLevel();
        void setZoomLevel(int zoomValue);
        void setZoomingMode(int zoomingModeToSet);
        QString zoomingModeString();
        void gotUser(MisterxUser * user, QString jid);


signals:
        void reloadMap();

private:
        QNetworkAccessManager *manager;

        int pendingRequests;
        int zoomingLevel;
        int zoomingMode;
        QString ownjid;
};

#endif // MAP_H
